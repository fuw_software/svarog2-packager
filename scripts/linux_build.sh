#!/usr/bin/env bash
cd dist
mkdir jre_temp_lin
tar xvzf ../build/jre/zulu11.37.17-ca-jre11.0.6-linux_x64.tar.gz -C jre_temp_lin
mv jre_temp_lin/zulu* jre_lin


mkdir svarog_linux
cp -r svarog svarog_linux/svarog
cp -r jre_lin svarog_linux/jre
cp ../build/mac/svarog.sh svarog_linux/svarog.sh
chmod +x svarog_linux/svarog.sh
