Download here:

https://gitlab.com/fuw_software/svarog2-packager/-/releases

# Building Svarog

This repo is created to build Svarog and release packages for Linux, Windows and MacOS. Main Svarog repo is a fork from Braintech and already contains building infrastucture used by Braintech. In FUW fork we want to maintain easy meging compatability with Braintech and decided to not touch the build script.

Svarog FUW fork is added into this repo as a submodule, to build a new version all you have to do is create a new commit which will point the `svarog2` submodule into the appropriate version you want to build.

Example assuming you don't have Svarog git cloned, in your terminal:

```
git clone https://gitlab.com/fuw_software/svarog2-packager.git
cd svarog2-packager
git submodule init
git submodule update
cd svarog2
git checkout SHA_OF_NEWEST_COMMIT
cd ..
git add svarog2
git commit -m "your notes here"
git push
```

The commit will be on development branch of the Svarog-packager and will start building immediately, which you'll be able to see in project pipelines.

# Making a release

Release will be published to https://gitlab.com/fuw_software/svarog2-packager/-/releases when you create an annotated tag in this repo. You should name the tag the same as `git describe --tags` result in the Svarog2 submodule.

Example, assuming you are in `svarog-packager` folder in your terminal:

```
cd svarog2
git describe --tags
3.0.1-44-g8121b81e
```

Use the output to createa a tag in the Gitlab webui.


